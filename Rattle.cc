//#include <boost/python.hpp>
//#include <hoomd/hoomd_config.h>
//#include <hoomd/VectorMath.h>
//#include "boost/python/extract.hpp"
//#include "boost/python/numeric.hpp"

#include <stdio.h>
#include <vector>

#include "Rattle.h"

//#include "QuaternionMultiply.h"

//using namespace std;
//using namespace hpmc;
//using namespace hpmc::detail;
//using namespace boost::python;


char *write_tuple(vec3<Scalar> vvv, char *s){
  sprintf(s, "<%g, %g, %g>", vvv.x, vvv.y, vvv.z);
  return s;
}

//Rattle::Rattle( boost::python::numeric::array _positions, boost::python::numeric::array _orientations) {
//Rattle::Rattle( boost::python::object _positions, boost::python::numeric::array _orientations) {
Rattle::Rattle(boost::python::numeric::array _box_dim, int _n_shapes, int _n_verts):n_shapes(_n_shapes), n_recorded_verts(0){
  
  boost::python::numeric::array box_dim = _box_dim;
  box_x = boost::python::extract<float>(box_dim[0]);
  box_y = boost::python::extract<float>(box_dim[1]);
  box_z = boost::python::extract<float>(box_dim[2]);
  m_verts.N = _n_verts;

} // end Rattle(...)

void Rattle::addVertex(boost::python::numeric::array _vertex){

  boost::python::numeric::array vertex = _vertex;
  float vertex_x = boost::python::extract<float>(vertex[0]);
  float vertex_y = boost::python::extract<float>(vertex[1]);
  float vertex_z = boost::python::extract<float>(vertex[2]);
  m_verts.x[n_recorded_verts] = vertex_x;
  m_verts.y[n_recorded_verts] = vertex_y;
  m_verts.z[n_recorded_verts] = vertex_z;
  n_recorded_verts++;
  if (n_recorded_verts > n_verts) fprintf(stderr, "Warning:  Adding more vertices than expected. %d/%d\n", n_recorded_verts, n_verts);

}

void Rattle::addShape(boost::python::numeric::array _position, boost::python::numeric::array _orientation){

  boost::python::numeric::array position = _position;
  float x = boost::python::extract<float>(position[0]);
  float y = boost::python::extract<float>(position[1]);
  float z = boost::python::extract<float>(position[2]);
  v_positions.push_back(vec3<float>(x,y,z));

  boost::python::numeric::array orientation = _orientation;
  float q_s = boost::python::extract<float>(orientation[0]);
  float q_x = boost::python::extract<float>(orientation[1]);
  float q_y = boost::python::extract<float>(orientation[2]);
  float q_z = boost::python::extract<float>(orientation[3]);
  v_orientations.push_back(quat<float>(q_s, vec3<float>(q_x, q_y, q_z)));

  if (v_positions.size() > n_shapes) fprintf(stderr, "Warning:  Adding more shapes than expected. %lu/%u\n", v_positions.size(), n_shapes);

}

/*
Rattle::Rattle(
      boost::python::numeric::array _positions, 
      boost::python::numeric::array _orientations, 
      boost::python::numeric::array _box_dim,
      boost::python::numeric::array _vertices, 
      int _n_shapes,  int n_vertices
    ): n_shapes(_n_shapes){

  // first sort out the box dimensions
  num_util::check_type(_box_dim, NPY_FLOAT);
  num_util::check_rank(_box_dim, 1);
  vec3<Scalar> *box_dim = (vec3<Scalar>*)num_util::data(_box_dim);
  box_x = box_dim->x;  box_y = box_dim->y;  box_z = box_dim->z;

  // now sort out the vertices
  num_util::check_type(_vertices, NPY_FLOAT);
  num_util::check_rank(_vertices, 2);
  vec3<Scalar>* vertices = (vec3<Scalar>*)num_util::data(_vertices);
  verts.N = n_vertices;
  Scalar diameter = 0, _diameter = 0;
  for(int i=0; i<n_vertices; i++) {
    verts.x[i] = vertices[i].x;
    verts.y[i] = vertices[i].y;
    verts.z[i] = vertices[i].z;
    // verts.v[i] = vec3<Scalar>(s1, s2, s3);
fprintf(stderr, "size of OverlapReal, Scalar, vec3<Scalar> = %d, %d, %d\n", sizeof(OverlapReal), sizeof(Scalar), sizeof(vec3<Scalar>));
fprintf(stderr, "vertices[%d].{x,y,z} = {%g, %g, %g}\n", i, vertices[i].x, vertices[i].y, vertices[i].z);
    // verts.v[i] = *vertices[i];
    _diameter = sqrt(verts.x[i] * verts.x[i] + verts.y[i] * verts.y[i] + verts.z[i] * verts.z[i]);
    if (_diameter > diameter) diameter = _diameter;
  }
fprintf(stderr, "setting diamter=%f\n", diameter);
  verts.diameter = diameter;

*/

void Rattle::finalizeConfiguration(){
  // process positions and orientations recieved from python
  // First n_shapes is original shape, wil add 7 boxes (X, Y, Z, XY, XZ, YZ, XYZ) to apply periodic boundary and store replicas in v_positions.
  
  // num_util::check_type(_positions, NPY_FLOAT);
  // num_util::check_rank(_positions, 2);
  // vec3<Scalar>* positions = (vec3<Scalar>*)num_util::data(_positions);

  // num_util::check_type(_orientations, NPY_FLOAT);
  // num_util::check_rank(_orientations, 2);
  // quat<Scalar>* orientations = (quat<Scalar>*)num_util::data(_orientations);

  // identity, original shapes
  // for (unsigned int i=0; i<n_shapes; i++){
  //   v_positions.push_back(positions[i]);
  //   v_orientations.push_back(orientations[i]);
  // }

  if (n_recorded_verts < m_verts.N) {
    printf("Cannot finalize configuration, there are only %d/%d vertices recorded.\n", n_recorded_verts, m_verts.N);
    return;
  }

  // Apply X half-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

  // Apply Y half-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

  // Apply Z half-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

  // Apply XY quarter-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

    // Apply XZ quarter-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

    // Apply YZ quarter-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

    // Apply XYZ eighth-box replicas
  for (unsigned int i=0; i<n_shapes; i++){
    vec3<Scalar> mirror_position = v_positions[i];
    if (mirror_position.x < 0) mirror_position.x += box_x; else mirror_position.x -= box_x; 
    if (mirror_position.y < 0) mirror_position.y += box_y; else mirror_position.y -= box_y; 
    if (mirror_position.z < 0) mirror_position.z += box_z; else mirror_position.z -= box_z; 
    v_positions.push_back(mirror_position);
    v_orientations.push_back(v_orientations[i]);
  }

  // This will do all the SCP's
  for (unsigned int i=0; i<8*n_shapes; i++)  v_scp.push_back(new hpmc::ShapeConvexPolyhedron(v_orientations[i], m_verts));

  // now set a few other details
  // vv_recorded_points will store the list of rattled points
  vv_recorded_points = new std::vector<std::vector<vec3<Scalar>*>*>();
  for (unsigned int i=0; i<n_shapes; i++) vv_recorded_points->push_back(new std::vector<vec3<Scalar>*>()); // initial points lists with empty vectors;

  setResolution();
  setVerletRadius();

  output_file = fopen("/tmp/Scene.pov", "a");

} // finalizeConfiguration()


/*
Rattle::~Rattle(){
  //std::cout << "destructing Rattle " << endl;
  // need to delete the objects in all them vectors....
}
*/

/*
void Rattle::printSingleShapeSDL(unsigned int shape_of_interest){

  char *color    = " rgb<1,0.5,0.25> ";
  char *transmit = " transmit 0.5 ";
  char *finish   = " finish {phong 1} ";

  vec3<Scalar> v0 = applyRotation(vec3<Scalar>(verts.x[0], verts.y[0], verts.z[0]), v_orientations[shape_of_interest]);
  vec3<Scalar> v1 = applyRotation(vec3<Scalar>(verts.x[1], verts.y[1], verts.z[1]), v_orientations[shape_of_interest]);
  vec3<Scalar> v2 = applyRotation(vec3<Scalar>(verts.x[2], verts.y[2], verts.z[2]), v_orientations[shape_of_interest]);
  vec3<Scalar> v3 = applyRotation(vec3<Scalar>(verts.x[3], verts.y[3], verts.z[3]), v_orientations[shape_of_interest]);

  // add the positions... 
  v0 = v0 + v_positions[shape_of_interest];
  v1 = v1 + v_positions[shape_of_interest];
  v2 = v2 + v_positions[shape_of_interest];
  v3 = v3 + v_positions[shape_of_interest];

  v0.x += positions[shape_of_interest].x; v0.y += positions[shape_of_interest].y; v0.z += positions[shape_of_interest].z;
  v1.x += positions[shape_of_interest].x; v1.y += positions[shape_of_interest].y; v1.z += positions[shape_of_interest].z;
  v2.x += positions[shape_of_interest].x; v2.y += positions[shape_of_interest].y; v2.z += positions[shape_of_interest].z;
  v3.x += positions[shape_of_interest].x; v3.y += positions[shape_of_interest].y; v3.z += positions[shape_of_interest].z;
  
  // four vertices, taken three at a time, will give me triangles which can then be used to generate pov info.
  char s1[256],s2[256],s3[256]; // string buffers for use by write_tuple. Need three, otherwise single buffer is overwritten before print finishes.
  
  printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v2, s3), color, transmit, finish );
  printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v3, s3), color, transmit, finish );
  printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
  printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v1, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
	    

  fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v2, s3), color, transmit, finish );
  fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v3, s3), color, transmit, finish );
  fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
  fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v1, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
}
*/


void Rattle::setResolution(Scalar _resolution){
  resolution = _resolution;
}

void Rattle::setVerletRadius(Scalar _verlet){
  verlet_sq = _verlet * _verlet;
}



void Rattle::buildVerletList(unsigned int shape_of_interest){

  // clear old Verlet lists
  memset(verlet_indices, 0, sizeof(verlet_indices));
  index_of_index = 0; // to add members to list

  // Notes on Verlet list and periodic boundary: 
  //   - the position list includes locations of periodic images
  //   - the shape list does not!
  //   - the Verlet shape list may have redundant pointers because of different images
  //   - the Verlet position list will not.
fprintf(stderr, "building verlet list for element at <%f,%f,%f>:\n", v_positions[shape_of_interest].x, v_positions[shape_of_interest].y, v_positions[shape_of_interest].z); fflush(stderr);
fprintf(stderr, "with orientations <%f,<%f,%f,%f> >:\n", v_orientations[shape_of_interest].s, v_orientations[shape_of_interest].v.x, v_orientations[shape_of_interest].v.y, v_orientations[shape_of_interest].v.z); fflush(stderr);
//printSingleShapeSDL(shape_of_interest);
  
  for (unsigned int i=0; i<8*n_shapes; i++) { // build a verlet list around shape of interest
    if (i==shape_of_interest) continue; // cos we don't count ourself

    Scalar dx = v_positions[i].x - v_positions[shape_of_interest].x; 
    Scalar dy = v_positions[i].y - v_positions[shape_of_interest].y; 
    Scalar dz = v_positions[i].z - v_positions[shape_of_interest].z; 
    if ((dx*dx + dy*dy + dz*dz) < verlet_sq) {
      verlet_indices[index_of_index++] = i; // add the shape to the list.
fprintf(stdout, "Added element at <%f,%f,%f> as Verlet list element #%u.\n", v_positions[i].x, v_positions[i].y, v_positions[i].z, index_of_index-1); fflush(stdout);
//printSingleShapeSDL(i);
    }

  } // end loop over n_shapes;  done building Verlet list

fprintf(stderr, "Verlet list size is %d\n", index_of_index); fflush(stderr);

}



void Rattle::rattleShape(unsigned int shape_of_interest){

fprintf(stderr, "rattling #%u\n", shape_of_interest);

  buildVerletList(shape_of_interest);

  // clear recursion_matrix, use 
  memset(recursion_matrix, 0, sizeof(recursion_matrix));

  // initiate recursive search of space
  visit(shape_of_interest, 128, 128, 128);

} // end rattleShape()



void Rattle::visit(unsigned int shape_of_interest, int _i, int _j, int _k) {

  if ((int)_i < 0 || (int)_j < 0 || (int)_k < 0) {printf("out of bounds\n"); return;}

  if (recursion_matrix[_i][_j][_k]) return; // already  visited
  recursion_matrix[_i][_j][_k] = 1; // mark the visited point
fprintf(stderr, "visiting %d::%d::%d\n", _i, _j, _k); fflush(stderr);

  hpmc::ShapeConvexPolyhedron scp_of_interest = *v_scp[shape_of_interest]; // retrieve the shape object of interest

  // stroll through Verlet list
  for (unsigned int i_verlet=0; i_verlet<index_of_index; i_verlet++) {

    hpmc::ShapeConvexPolyhedron scp = *v_scp[verlet_indices[i_verlet]]; // retrieve the shape object i_shape
    vec3<Scalar> r_ab; 
    // vec3<Scalar> r_ab = v_positions[shape_of_interest] - v_positions[verlet_indices[i_verlet]]; 
    r_ab.x = v_positions[shape_of_interest].x - v_positions[verlet_indices[i_verlet]].x + (_i - 128) * resolution;
    r_ab.y = v_positions[shape_of_interest].y - v_positions[verlet_indices[i_verlet]].y + (_j - 128) * resolution;
    r_ab.z = v_positions[shape_of_interest].z - v_positions[verlet_indices[i_verlet]].z + (_k - 128) * resolution;
    unsigned int err;
    
    // need to do something different here... returns null pointer if no rattle space?
    // if (test_overlap<ShapeConvexPolyhedron,ShapeConvexPolyhedron>(r_ab, scp, scp_of_interest, err)) return;

    int overlap=hpmc::test_overlap<hpmc::ShapeConvexPolyhedron,hpmc::ShapeConvexPolyhedron>(r_ab, scp, scp_of_interest, err); 
//fprintf(stderr, "...%d[[%d]]", i_verlet, err);fflush(stderr);
if (overlap) printf("overlap at %d,%d,%d\n", _i, _j, _k);
    if (overlap) return;

  } // end for i_shape loop over verlet

  // record it
  (vv_recorded_points->at(shape_of_interest))->push_back(new vec3<Scalar>(
    v_positions[shape_of_interest].x + (_i - 128) * resolution, 
    v_positions[shape_of_interest].y + (_j - 128) * resolution, 
    v_positions[shape_of_interest].z + (_k - 128) * resolution
  ));

  // visit each neighbor
  visit(shape_of_interest, _i - 1, _j, _k); visit(shape_of_interest, _i + 1, _j, _k);
  visit(shape_of_interest, _i, _j - 1, _k); visit(shape_of_interest, _i, _j + 1, _k);
  visit(shape_of_interest, _i, _j, _k - 1); visit(shape_of_interest, _i, _j, _k + 1);

}


/*
void Rattle::printSDL(int shape_of_interest){
fprintf(stderr, "writing SDL...\n");
fprintf(stderr, "size of vector is %ld...\n", vv_recorded_points->at(shape_of_interest)->size());
  std::vector<vec3<Scalar>*> *v_shape_of_interest = vv_recorded_points->at(shape_of_interest); 
  for (vector<vec3<Scalar>*>::iterator it = v_shape_of_interest->begin(); it != v_shape_of_interest->end(); ++it){
    printf("<%f,%f,%f>\n", (*it)->x, (*it)->y, (*it)->z);
    fprintf(output_file, "<%f,%f,%f>\n", (*it)->x, (*it)->y, (*it)->z);
  }
  fflush(stdout);
  fflush(output_file);
}
*/

/*
void Rattle::printVerletSDL(int shape_of_interest){
fprintf(stderr, "writing Verlet SDL...\n");

  buildVerletList(shape_of_interest);

  // for (vector<vec3<Scalar>*>::iterator it = v_shape_of_interest->begin(); it != v_shape_of_interest->end(); ++it){

  // for (vector<vec3<Scalar>*>::iterator it = v_verlet_positions.begin(); it != v_verlet_positions.end(); ++it){
    // v_verlet_scp.push_back(v_scp[i]);
    // v_verlet_positions.push_back(v_positions[i + n_shapes*periodic_image]);

    // printf("positions: <%f, %f, %f>\n", (*it)->x, (*it)->y, (*it)->z);

 // how do i get the correct orientation? 
 // v_verlet_scp[i] contains the scp which contains the orientation
 /*
    vec3<Scalar> v0 = applyRotation(vec3<Scalar>(verts.x[0], verts.y[0], verts.z[0]), orientations[shape_of_interest]);
    vec3<Scalar> v1 = applyRotation(vec3<Scalar>(verts.x[1], verts.y[1], verts.z[1]), orientations[shape_of_interest]);
    vec3<Scalar> v2 = applyRotation(vec3<Scalar>(verts.x[2], verts.y[2], verts.z[2]), orientations[shape_of_interest]);
    vec3<Scalar> v3 = applyRotation(vec3<Scalar>(verts.x[3], verts.y[3], verts.z[3]), orientations[shape_of_interest]);

    // add the positions... 
    v0.x += positions[shape_of_interest].x; v0.y += positions[shape_of_interest].y; v0.z += positions[shape_of_interest].z;
    v1.x += positions[shape_of_interest].x; v1.y += positions[shape_of_interest].y; v1.z += positions[shape_of_interest].z;
    v2.x += positions[shape_of_interest].x; v2.y += positions[shape_of_interest].y; v2.z += positions[shape_of_interest].z;
    v3.x += positions[shape_of_interest].x; v3.y += positions[shape_of_interest].y; v3.z += positions[shape_of_interest].z;
	    
    // four vertices, taken three at a time, will give me triangles which can then be used to generate pov info.
    char s1[256],s2[256],s3[256]; // string buffers for use by write_tuple. Need three, otherwise single buffer is overwritten before print finishes.
    
    printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v2, s3), color, transmit, finish );
    printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v3, s3), color, transmit, finish );
    printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
    printf("triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v1, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
	    
    fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v2, s3), color, transmit, finish );
    fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v1, s2), write_tuple(v3, s3), color, transmit, finish );
    fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v0, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );
    fprintf(output_file, "triangle { %s, %s, %s texture{ pigment{ color %s %s } %s }}\n", write_tuple(v1, s1), write_tuple(v2, s2), write_tuple(v3, s3), color, transmit, finish );

  // }
	    
  fflush(stdout);
  fflush(output_file);
}
*/


/*
void Rattle::finalize(){
  fclose(output_file);
}
*/





// void Rattle::getVals(boost::python::numeric::array data){
  // first sort out the box dimensions
  //num_util::check_type(_box_dim, NPY_FLOAT);
  //num_util::check_rank(_box_dim, 1);
  //vec3<Scalar> *box_dim = (vec3<Scalar>*)num_util::data(_box_dim);
  //Scalar box_x = _box_dim[0];
  //Scalar box_y = _box_dim[1];
  //Scalar box_z = _box_dim[2];
  
  // note that there is a converter for float64 but not float32. brilliant. i.e.:
  // >>> box_dim = np.array([3.3, 4.4, 5.5],dtype=float64)
  
  // boost::python::numeric::array a = data;
	
  // float x = boost::python::extract<float>(a[0]);
  // printf("got box_dim as %f \n", x);
// }


