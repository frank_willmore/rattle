// Rattle.h

#include <hoomd/hoomd_config.h>
#include <hoomd/HOOMDMath.h>
#include <HPMCPrecisionSetup.h>
#include <hoomd/VectorMath.h>
#include <ShapeConvexPolyhedron.h>
//#include "poly3d_verts.h"

#include <boost/python.hpp>
#include <iostream>
//#include "QuaternionMultiply.h"
#include <vector>

#ifndef __RATTLE_H__
#define __RATTLE_H__

#define RATTLE_VERLET_MAX 1024

//using namespace boost::python;
//using namespace hpmc::detail;
//using namespace hpmc;


class Rattle{

    hpmc::detail::poly3d_verts m_verts;
    std::vector<vec3<Scalar> > v_positions;
    std::vector<quat<Scalar> > v_orientations;
    std::vector<hpmc::ShapeConvexPolyhedron*> v_scp;
    int verlet_indices[RATTLE_VERLET_MAX];
    unsigned int index_of_index; // to add members to list
    unsigned int recursion_matrix[256][256][256];
    Scalar resolution;
    Scalar verlet_sq;
    unsigned int n_shapes;
    unsigned int n_verts;
    unsigned int n_recorded_verts;
    Scalar box_x, box_y, box_z;
    std::vector<std::vector<vec3<Scalar>*>*> *vv_recorded_points;
    FILE *output_file;

    void visit(unsigned int shape_of_interest, int _i, int _j, int _k);
    void buildVerletList(unsigned int shape_of_interest);
    void printSingleShapeSDL(unsigned int shape_of_interest);

  public:

    Rattle(boost::python::numeric::array _box_dim, int _n_shapes, int _n_verts);
    void addVertex(boost::python::numeric::array _vertex);
    void addShape(boost::python::numeric::array _position, boost::python::numeric::array _orientation);
    void finalizeConfiguration();

    void rattleShape(unsigned int shape_of_interest);
    //void printSDL(int shape_of_interest);
    //void printVerletSDL(int shape_of_interest);
    void setResolution(Scalar _resolution = 0.1);
    void setVerletRadius(Scalar _verlet = 4.0);

}; // end class Rattle

#endif // __RATTLE_H__

