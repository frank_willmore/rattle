#include <boost/python.hpp>
#include <hoomd/hoomd_config.h>
#include <hoomd/VectorMath.h>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
#include <stdio.h>

#include "Rattle.h"

using namespace boost::python;

/*
class Rattle{
public:
  Rattle(boost::python::numeric::array data);
};
*/

//void getVals(boost::python::numeric::array data){
  // first sort out the box dimensions
  //num_util::check_type(_box_dim, NPY_FLOAT);
  //num_util::check_rank(_box_dim, 1);
  //vec3<Scalar> *box_dim = (vec3<Scalar>*)num_util::data(_box_dim);
  //Scalar box_x = _box_dim[0];
  //Scalar box_y = _box_dim[1];
  //Scalar box_z = _box_dim[2];
  
  // note that there is a converter for float64 but not float32. brilliant. i.e.:
  // >>> box_dim = np.array([3.3, 4.4, 5.5],dtype=float64)
  
  //boost::python::numeric::array a = data;
	
  //float x = extract<float>(a[0]);
  //printf("got box_dim as %f \n", x);
//}

/*
Rattle::Rattle(
//    boost::python::numeric::array _offsets,
//    boost::python::numeric::array _orientations,
    boost::python::numeric::array _box_dim
//    boost::python::numeric::array _vertices,
//    int _n_shapes,  int n_vertices
    ) //: n_shapes(_n_shapes)
    {
  boost::python::numeric::array a = _box_dim;
	
  float x = extract<float>(a[0]);
  printf("got Rattle::Rattle as %f \n", x);
    }
*/

				    
BOOST_PYTHON_MODULE(rattle)
{
    //def("getVals", getVals);
    boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
    //class_<Rattle>("Rattle",  init<boost::python::numeric::array>());
    class_<Rattle>("Rattle",  init<boost::python::numeric::array, int, int>())
    .def("addVertex", &Rattle::addVertex)
    .def("addShape", &Rattle::addShape)
    .def("rattleShape", &Rattle::rattleShape)
    .def("setVerletRadius", &Rattle::setVerletRadius)
    .def("setResolution", &Rattle::setResolution)
    .def("finalizeConfiguration", &Rattle::finalizeConfiguration);
}


