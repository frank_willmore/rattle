# location of the Python header files
 
HPMC_INCLUDE=/home/frankted/opt/hoomd-plugin-hpmc/cppmodule
HOOMD_INCLUDE=/home/frankted/hoomd-install/include
CUDA_INCLUDE=/opt/cuda/include
BOOST_INCLUDE=/usr/include
NUMPY_INCLUDE=/usr/lib64/python3.3/site-packages/numpy/core/include/

PYTHON_VERSION = 3.3
PYTHON_INCLUDE = /usr/include/python$(PYTHON_VERSION)
 
# location of the Boost Python include files and library
 
BOOST_INC = /usr/include
BOOST_LIB = /usr/lib
 
# compile mesh classes

$rattle.so: rattle_module.o Rattle.o
	g++ -shared -Wl,--export-dynamic /usr/lib64/python3.3/site-packages/numpy/core/lib/libnpymath.a Rattle.o rattle_module.o -L$(BOOST_LIB) -lboost_python-$(PYTHON_VERSION) -L/usr/lib/python$(PYTHON_VERSION)/config -lpython$(PYTHON_VERSION) -o rattle.so
 
rattle_module.o: rattle_module.cc
	g++ -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(HPMC_INCLUDE) -I$(HOOMD_INCLUDE) -I$(CUDA_INCLUDE) -I$(NUMPY_INCLUDE) -fPIC -c rattle_module.cc

Rattle.o: Rattle.cc
	g++ -I$(PYTHON_INCLUDE) -I$(BOOST_INC) -I$(HPMC_INCLUDE) -I$(HOOMD_INCLUDE) -I$(CUDA_INCLUDE) -I$(NUMPY_INCLUDE) -fPIC -c Rattle.cc

clean:
	rm -f *.so *.o

test: test.cc
	clang++ test.cc -I/home/frankted/opt/hoomd-plugin-hpmc/cppmodule -I/home/frankted/hoomd-install/include -I/opt/cuda/include -L/usr/lib -lmpi -lmpi_cxx -o test
